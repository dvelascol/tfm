#!/bin/bash


usage(){

	echo "Para ejecutar el ataque: ./hardening.sh <switch.conf>"	
}

if [[ $UID != 0 ]]
then
	echo "Please, run as sudo"
	exit 1
fi

# Se pasa fichero de config de conmutador y fichero de config de servidor tftp
if [[ $# -ne 2 ]]
then
	usage
fi

which apt >/dev/null
if [ $? -eq 0 ]
then
    echo "Instalando servidor TFTP"
else
	echo "Este script solo funciona para distribuciones basadas en Debian"
	exit 1
fi

which tftp
if [ $? -eq 1 ]
then

	sudo apt update
	sudo apt install tftpd-hpa -y
fi

sudo cp $2 /etc/default/httpd-hpa

sudo systemctl restart tftpd-hpa.service

sudo cp $1 /srv/tftp/


